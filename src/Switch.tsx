import React from 'react'
import styles from './Switch.module.css'

type Props = {
  id: string
  value: boolean
  onChange: () => void
}

export function Switch(props: Props) {
  const { id, value, onChange } = props
  return (
    <span className={styles.wrapper}>
      <input
        id={id}
        className={styles.input}
        type="checkbox"
        checked={value}
        onChange={() => onChange()}
      />
      <label htmlFor={id} className={value ? styles.label : styles.checkedLabel} />
    </span>
  )
}
