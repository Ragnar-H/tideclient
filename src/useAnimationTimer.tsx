import { useState, useEffect } from 'react'

// https://usehooks.com/useAnimation/
export function useAnimationTimer(duration = 1000, delay = 0) {
  const [elapsed, setTime] = useState(0)

  useEffect(() => {
    let animationFrame: number, timerStop: () => void, start: number

    // Function to be executed on each animation frame

    function onFrame() {
      setTime(Date.now() - start)

      loop()
    }

    // Call onFrame() on next animation frame

    function loop() {
      animationFrame = requestAnimationFrame(onFrame)
    }

    function onStart() {
      // Set a timeout to stop things when duration time elapses

      timerStop = () => {
        cancelAnimationFrame(animationFrame)

        setTime(Date.now() - start)
      }

      // Start the loop

      start = Date.now()

      loop()
    }

    // Start after specified delay (defaults to 0)

    const timerDelay = setTimeout(onStart, delay)

    // Clean things up

    return () => {
      timerStop()

      clearTimeout(timerDelay)

      cancelAnimationFrame(animationFrame)
    }
  }, [duration, delay]) // Only re-run effect if duration or delay changes

  return elapsed
}
