import React from 'react'
import Slider from 'react-smooth-range-input'
import { primaryLightColor } from './theme'

type Props = {
  value: number
  min: number
  max: number
  onChange: (number: any) => void
}

export function RangeInput(props: Props) {
  const { value, min, max, onChange } = props
  return (
    <Slider
      key={max}
      value={value}
      min={min}
      max={max}
      onChange={onChange}
      barColor={primaryLightColor}
    />
  )
}
