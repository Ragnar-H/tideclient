/* eslint-disable @typescript-eslint/camelcase */
import React from 'react'

import { storiesOf } from '@storybook/react'
import { Dashboard } from '../Dashboard'
import { TideDatePoint } from '../dtos'
import { action } from '@storybook/addon-actions'

const raw_data: TideDatePoint[] = [
  {
    tide_level_mm: 1530.0,
    timestamp: '2019-06-16T03:20:00',
  },
  {
    tide_level_mm: 2830.0,
    timestamp: '2019-06-16T09:20:00',
  },
  {
    tide_level_mm: 1690.0,
    timestamp: '2019-06-16T15:04:00',
  },
  {
    tide_level_mm: 3080.0,
    timestamp: '2019-06-16T21:08:00',
  },
  {
    tide_level_mm: 1460.0,
    timestamp: '2019-06-17T03:28:00',
  },
  {
    tide_level_mm: 2860.0,
    timestamp: '2019-06-17T09:29:00',
  },
  {
    tide_level_mm: 1630.0,
    timestamp: '2019-06-17T15:13:00',
  },
  {
    tide_level_mm: 3160.0,
    timestamp: '2019-06-17T21:20:00',
  },
  {
    tide_level_mm: 1410.0,
    timestamp: '2019-06-18T03:41:00',
  },
  {
    tide_level_mm: 2890.0,
    timestamp: '2019-06-18T09:42:00',
  },
  {
    tide_level_mm: 1580.0,
    timestamp: '2019-06-18T15:26:00',
  },
  {
    tide_level_mm: 3220.0,
    timestamp: '2019-06-18T21:36:00',
  },
  {
    tide_level_mm: 1380.0,
    timestamp: '2019-06-19T03:58:00',
  },
  {
    tide_level_mm: 2900.0,
    timestamp: '2019-06-19T09:58:00',
  },
  {
    tide_level_mm: 1550.0,
    timestamp: '2019-06-19T15:44:00',
  },
  {
    tide_level_mm: 3240.0,
    timestamp: '2019-06-19T21:56:00',
  },
  {
    tide_level_mm: 1400.0,
    timestamp: '2019-06-20T04:18:00',
  },
  {
    tide_level_mm: 2890.0,
    timestamp: '2019-06-20T10:18:00',
  },
  {
    tide_level_mm: 1560.0,
    timestamp: '2019-06-20T16:05:00',
  },
  {
    tide_level_mm: 3200.0,
    timestamp: '2019-06-20T22:19:00',
  },
  {
    tide_level_mm: 1460.0,
    timestamp: '2019-06-21T04:43:00',
  },
  {
    tide_level_mm: 2820.0,
    timestamp: '2019-06-21T10:42:00',
  },
  {
    tide_level_mm: 1630.0,
    timestamp: '2019-06-21T16:30:00',
  },
  {
    tide_level_mm: 3090.0,
    timestamp: '2019-06-21T22:47:00',
  },
  {
    tide_level_mm: 1590.0,
    timestamp: '2019-06-22T05:14:00',
  },
  {
    tide_level_mm: 2700.0,
    timestamp: '2019-06-22T11:15:00',
  },
  {
    tide_level_mm: 1790.0,
    timestamp: '2019-06-22T17:02:00',
  },
  {
    tide_level_mm: 2890.0,
    timestamp: '2019-06-22T23:23:00',
  },
  {
    tide_level_mm: 1790.0,
    timestamp: '2019-06-23T06:00:00',
  },
  {
    tide_level_mm: 2500.0,
    timestamp: '2019-06-23T12:09:00',
  },
  {
    tide_level_mm: 2040.0,
    timestamp: '2019-06-23T17:50:00',
  },
  {
    tide_level_mm: 2600.0,
    timestamp: '2019-06-24T00:27:00',
  },
  {
    tide_level_mm: 2000.0,
    timestamp: '2019-06-24T08:19:00',
  },
  {
    tide_level_mm: 2430.0,
    timestamp: '2019-06-24T16:29:00',
  },
  {
    tide_level_mm: 2140.0,
    timestamp: '2019-06-24T22:46:00',
  },
  {
    tide_level_mm: 2570.0,
    timestamp: '2019-06-25T04:55:00',
  },
  {
    tide_level_mm: 1770.0,
    timestamp: '2019-06-25T11:31:00',
  },
  {
    tide_level_mm: 2840.0,
    timestamp: '2019-06-25T18:03:00',
  },
]

storiesOf('Dashboard', module).add('default', () => (
  <Dashboard data={raw_data} maxDays={40} getMoreData={action('getMoreData')} />
))
