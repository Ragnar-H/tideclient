import React from 'react'

import { storiesOf } from '@storybook/react'
import { RangeInput } from '../RangeInput'
import { action } from '@storybook/addon-actions'

storiesOf('RangeInput', module).add('default', () => (
  <div
    style={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '100vh',
    }}
  >
    <RangeInput value={2} min={1} max={15} onChange={action('onChange')} />
  </div>
))
