import React, { useState } from 'react'

import { storiesOf } from '@storybook/react'
import { Switch } from '../Switch'
import { action } from '@storybook/addon-actions'

function SwitchContainer() {
  const [value, setValue] = useState(true)
  return (
    <Switch id="some-id" value={value} onChange={() => setValue(oldValue => !oldValue)} />
  )
}

storiesOf('Switch', module)
  .add('default', () => (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
      }}
    >
      <Switch id="some-id" value={false} onChange={action('onChange')} />
    </div>
  ))
  .add('interactive', () => (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
      }}
    >
      <SwitchContainer />
    </div>
  ))
