/* eslint-disable @typescript-eslint/camelcase */
import React from 'react'

import { storiesOf } from '@storybook/react'
import { TideGraph } from '../TideGraph'
import { TideDatePoint } from '../dtos'

const raw_data: TideDatePoint[] = [
  {
    tide_level_mm: 1530.0,
    timestamp: '2019-06-16T03:20:00',
  },
  {
    tide_level_mm: 2830.0,
    timestamp: '2019-06-16T09:20:00',
  },
  {
    tide_level_mm: 1690.0,
    timestamp: '2019-06-16T15:04:00',
  },
  {
    tide_level_mm: 3080.0,
    timestamp: '2019-06-16T21:08:00',
  },
]

storiesOf('TideGraph', module)
  .add('default', () => (
    <div style={{ height: '100vh', width: '100vw' }}>
      <TideGraph data={raw_data} isAnimated={false} />
    </div>
  ))
  .add('animated', () => (
    <div style={{ height: '100vh', width: '100vw' }}>
      <TideGraph data={raw_data} isAnimated={true} />
    </div>
  ))
