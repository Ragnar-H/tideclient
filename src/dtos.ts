export type TidesResponse = {
  days_forecasted: number
  tide_levels: TideDatePoint[]
}

export type TideDatePoint = {
  timestamp: string
  tide_level_mm: number
}
