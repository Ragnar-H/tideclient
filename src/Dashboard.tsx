import React, { useState, useEffect } from 'react'
import { DateTime } from 'luxon'
import { TideDatePoint } from './dtos'
import styles from './Dashboard.module.css'
import { TideGraph } from './TideGraph'
import { RangeInput } from './RangeInput'
import { Switch } from './Switch'

function getSlicedDataInRange(
  startDate: string,
  daysFromNow: number,
  data: TideDatePoint[]
) {
  const timestampToDate = DateTime.fromISO(startDate)
  const endDate = timestampToDate.plus({ days: daysFromNow })

  const dataInRange = data.filter(dataPoint => {
    const { days: daysDiff } = DateTime.fromISO(dataPoint.timestamp)
      .diff(endDate, 'days')
      .shiftTo('days')
    return daysDiff <= 0
  })

  return dataInRange
}

type Props = {
  data: TideDatePoint[]
  maxDays: number
  getMoreData: (daysFromNow: number) => void
}

export function Dashboard(props: Props) {
  const { data, maxDays, getMoreData } = props
  const [daysFromNow, setDaysFromNow] = useState(1)
  const [isAnimated, setIsAnimated] = useState(false)
  const slicedData =
    data.length > 0 ? getSlicedDataInRange(data[0].timestamp, daysFromNow, data) : []

  const { days: maxDayInData } =
    data.length > 0
      ? DateTime.fromISO(data[data.length - 1].timestamp)
          .diffNow('days')
          .shiftTo('days')
      : { days: 0 }

  useEffect(() => {
    if (maxDayInData - daysFromNow <= -1) {
      getMoreData(daysFromNow)
    }
  }, [maxDayInData, daysFromNow, getMoreData])

  return (
    <div className={isAnimated ? styles.containerAnimated : styles.container}>
      <div>
        <h1 className={styles.headLine}>Tide levels in Akranes</h1>
      </div>
      <div className={styles.stackedCentered}>
        <div className={styles.stackedCentered}>
          <RangeInput
            value={daysFromNow}
            min={1}
            max={maxDays}
            onChange={setDaysFromNow}
          />
          <p>Number of days forecasted</p>
        </div>
        <div className={styles.stackedCentered}>
          <Switch
            id="animated"
            value={isAnimated}
            onChange={() => setIsAnimated(oldValue => !oldValue)}
          />
          <p>{isAnimated ? 'Turn off animations' : 'Add some waves'}</p>
        </div>
      </div>
      <div className={styles.graphContainer}>
        <TideGraph data={slicedData} isAnimated={isAnimated} />
      </div>
    </div>
  )
}
