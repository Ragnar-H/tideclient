// Sigh, we need to manually keep this up to date with `index.css` :(
export const primaryColor = '#1e88e5'
export const primaryLightColor = '#6ab7ff'
export const primaryDarkColor = '#005cb2'
export const secondaryColor = '#aed581'
export const secondaryLightColor = '#e1ffb1'
export const secondaryDarkColor = '#7da453'
export const primaryTextColor = '#f5f5f5'
export const secondaryTextColor = '#212121'
