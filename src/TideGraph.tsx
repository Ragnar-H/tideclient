import React from 'react'
import { ResponsiveLine } from '@nivo/line'
import { TideDatePoint } from './dtos'
import { primaryColor, primaryDarkColor } from './theme'
import { useAnimation } from './useAnimation'

const WAVE_DIFF = 300
const ANIMATION_DURATION = 1800
const ANIMATION_DELAY = 300

type Props = {
  data: TideDatePoint[]
  isAnimated: boolean
}

export function TideGraph(props: Props) {
  const { data, isAnimated } = props
  const animation = useAnimation('cubic', ANIMATION_DURATION, ANIMATION_DELAY)

  const animatedDiff = isAnimated ? WAVE_DIFF : 0
  const transformedData = [
    {
      id: 'Akranes',
      data: data.map((datum, index) => ({
        x: datum.timestamp,
        y:
          index % 2 === 0
            ? datum.tide_level_mm + animatedDiff * animation
            : datum.tide_level_mm - animatedDiff * animation,
      })),
    },
  ]

  return (
    <ResponsiveLine
      data={transformedData}
      isInteractive={true}
      useMesh={false}
      enableSlices="x"
      enablePointLabel={false}
      margin={{ top: 50, right: 60, bottom: 50, left: 60 }}
      xScale={{
        type: 'time',
        format: '%Y-%m-%dT%H:%M:%S',
        precision: 'hour',
      }}
      yScale={{
        type: 'linear',
        stacked: true,
        min: 'auto',
        max: 'auto',
      }}
      curve="natural"
      axisBottom={{
        format: '%b %d %H:%M',
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        tickValues: 2,
        legend: 'time',
        legendOffset: 0,
        legendPosition: 'middle',
      }}
      axisLeft={{
        tickSize: 5,
        tickPadding: 5,
        tickValues: 2,
        tickRotation: 0,
        legend: 'tide level (mm)',
        legendOffset: -50,
        legendPosition: 'middle',
      }}
      colors={isAnimated ? primaryDarkColor : primaryColor}
      enableGridX={false}
      enableGridY={false}
      enablePoints={false}
      enableArea={true}
    />
  )
}
