import React, { useState, useEffect } from 'react'
import { Dashboard } from './Dashboard'
import { TideDatePoint, TidesResponse } from './dtos'
import { DateTime } from 'luxon'
import { API_ENDPOINT } from './contants'

function useTideData(endTimestamp: string | null) {
  const [tideData, setTideData] = useState<TideDatePoint[]>([])
  const [maxDays, setMaxDays] = useState(0)
  useEffect(() => {
    if (endTimestamp) {
      let isFresh = true
      fetchTideData(endTimestamp).then(result => {
        if (isFresh) {
          setMaxDays(result.maxDays)
          setTideData(result.data)
        }
      })
      return () => {
        isFresh = false
      }
    }
  }, [endTimestamp])

  return { tideData, maxDays }
}

type TideDataCache = {
  maxDays: number
  data: { [key: string]: TideDatePoint }
}

const cache: TideDataCache = {
  maxDays: 0,
  data: {},
}
let latestTimestamp = DateTime.utc()
  .set({ millisecond: 0 })
  .toISO({ suppressMilliseconds: true })

type TransformedResponse = {
  maxDays: number
  data: TideDatePoint[]
}
async function fetchTideData(endTimestamp: string): Promise<TransformedResponse> {
  const latestDate = DateTime.fromISO(latestTimestamp)
  const requestedEndDate = DateTime.fromISO(endTimestamp)

  const isInCache = requestedEndDate.diff(latestDate).milliseconds < 0
  if (isInCache) {
    return {
      maxDays: cache.maxDays,
      data: Object.values(cache.data),
    }
  }

  return fetch(`${API_ENDPOINT}/tides/${latestTimestamp}/${endTimestamp}`)
    .then(res => res.json())
    .then((result: TidesResponse) => {
      cache.maxDays = result.days_forecasted

      result.tide_levels.forEach(tideData => {
        cache.data[tideData.timestamp] = tideData
        if (tideData.timestamp > latestTimestamp) {
          latestTimestamp = tideData.timestamp
        }
      })

      return {
        maxDays: cache.maxDays,
        data: Object.values(cache.data),
      }
    })
}

function App() {
  const [tideEndTimestamp, setTideEndTimestamp] = useState<string | null>(null)

  const { maxDays, tideData } = useTideData(tideEndTimestamp)

  function handleGetMoreData(daysFromNow: number) {
    const endTimestamp = DateTime.utc()
      .plus({
        days: daysFromNow,
      })
      .set({ millisecond: 0 })
      .toISO({ suppressMilliseconds: true })

    setTideEndTimestamp(endTimestamp)
  }
  return <Dashboard maxDays={maxDays} data={tideData} getMoreData={handleGetMoreData} />
}

export default App
