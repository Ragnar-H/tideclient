module.exports = {
    env: {
      browser: true,
      es6: true
    },
    extends: [
      "eslint:recommended",
      "plugin:react/recommended",
      "plugin:jest/recommended",
      "plugin:@typescript-eslint/recommended",
      "prettier",
      "prettier/@typescript-eslint",
      "prettier/react"
    ],
    settings: {
      react: {
        version: "detect"
      }
    },
    globals: {
      Atomics: "readonly",
      SharedArrayBuffer: "readonly"
    },
    parserOptions: {
      ecmaFeatures: {
        jsx: true
      },
      ecmaVersion: 2018,
      sourceType: "module"
    },
    plugins: ["jest", "react", "@typescript-eslint", "react-hooks", "prettier"],
    rules: {
      "prettier/prettier": [
        "error",
        {
          trailingComma: "es5",
          singleQuote: true,
          printWidth: 90,
          semi: false
        }
      ],
      "prefer-const": "error",
      "prefer-template": "error",
      "react-hooks/rules-of-hooks": "error",
      "react/prop-types": "off",
  
      
      "@typescript-eslint/explicit-function-return-type": "off",
      "@typescript-eslint/explicit-member-accessibility": "off",
      "@typescript-eslint/no-use-before-define": "off",
      "@typescript-eslint/array-type": "off",
  
      "@typescript-eslint/no-explicit-any": "off", // that seems to restrictive
      "@typescript-eslint/prefer-interface": "off", // you might not want people to extend your types
      "no-unused-vars": "off" // replaced by @typescript-eslint/no-unused-vars
    }
  };
  