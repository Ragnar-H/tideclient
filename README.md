This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Description

Display data from https://gitlab.com/Ragnar-H/tidescraper

![](demo.gif)

## Development

Get started
```
yarn install
yarn start
```

Storybook
```
yarn storybook
```

## App structure

The grunt of the work is done by `TideGraph` which is built with the [nivo](https://nivo.rocks/line) graphing library.

Data fetching is the responsiblity of `App.tsx` where a local cache ensures the client does not fetch the same data twice.
_Verify this by going a few days forward, this fires a network request, then going back again._

Some gimmicky animation is done using a slightly modified hook from https://usehooks.com/useAnimation/

## Improvements to be made

- Assert correct data fetching via tests
- More reusable styles; instead of a mix of [css-modules](https://github.com/css-modules/css-modules) and inline styles
- Central data storage; opted for a super-simple cache in a local object given the time limit. Adding something more production ready could take to form of [react context](https://kentcdodds.com/blog/application-state-management-with-react)
- Animate via styles instead of altering the data; This is not ideal because: We're changing the truth (data) and animations go through JS
- Defensive data fetching; Currently we assume the happy path of always getting a response. That's obviously not the case in real life.
